//
//  AppDelegate.h
//  DatabaseManager
//
//  Created by mac on 14-6-3.
//  Copyright (c) 2014年 VSIVS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
