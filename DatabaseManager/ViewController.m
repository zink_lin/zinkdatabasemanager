//
//  ViewController.m
//  DatabaseManager
//
//  Created by mac on 14-6-3.
//  Copyright (c) 2014年 VSIVS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionTest:(id)sender {
    ZinkDBManager * dbManager = [[ZinkDBManager alloc] init];
    
    [dbManager saveData:@[@"a", @"a", @"a", @"a", @"a", @"a", @"a", @"a", @"a", @"a"] toTable:@"testTable"];
//    [dbManager deleteAllFromTable:@"testTable"];
    NSLog(@"%@", [dbManager queryAllFromTable:@"testTable"]);
    
    [dbManager updateData:@{@"t1": @"s5o1", @"t2":@"q7"} ByCondition:@{@"t1":@"a"} fromTable:@"testtable"];
    [dbManager deleteWithCondition:@{@"t1": @"s5o1", @"t2":@"q7", @"t3": @"3"} fromTable:@"testTable"];
    NSLog(@"%@", [dbManager queryWithCondition:@{@"t2": @"q7"} fromTable:@"testTable"]);
    
    
}

@end
