//
//  ZinkDBManager.h
//  DatabaseManager
//
//  Created by mac on 14-6-3.
//  Copyright (c) 2014年 VSIVS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ZinkDBManager : NSObject

//要先导入 libsqlite3
///开启数据库
+(sqlite3 *)openDB;
///关闭数据库
+(void)closeDB;

//增
-(BOOL)saveData:(NSArray *)data toTable:(NSString *)tableName;
//删
-(BOOL)deleteWithCondition:(NSDictionary *)data fromTable:(NSString *)tableName;
-(BOOL)deleteAllFromTable:(NSString *)tableName;
//改
-(BOOL)updateData:(NSDictionary *)data ByCondition:(NSDictionary *)condition fromTable:(NSString *)tableName;
//查
-(NSMutableArray *)queryWithCondition:(NSDictionary *)data fromTable:(NSString *)tableName;
-(NSMutableArray *)queryAllFromTable:(NSString *)tableName;

@end
