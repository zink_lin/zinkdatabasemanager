//
//  ZinkDBManager.m
//  DatabaseManager
//
//  Created by mac on 14-6-3.
//  Copyright (c) 2014年 VSIVS. All rights reserved.
//

#import "ZinkDBManager.h"


@implementation ZinkDBManager

static sqlite3 * db = nil;

//开启数据库
+(sqlite3 *)openDB{
    if (db) {
        return db;
    }
    
    //原始路径,   请正确填写此处的数据库名称
    NSString * originPath = [[NSBundle mainBundle]pathForResource:@"TestDB" ofType:@"sqlite"];
    
    if (originPath.length == 0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"数据库名称未配置" message:@"请检测+openDB方法中的数据库文件名称" delegate:nil cancelButtonTitle:@"遵命，大王" otherButtonTitles:@"叫我女王大人！", @"好的，大王", @"知道了，大王",nil];
        [alert show];
        
        return nil;
    }
    
    //目标路径
    NSString * docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * sqlFilePath = [docPath stringByAppendingPathComponent:@"TestDB.sqlite"];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //如果doc下没有数据库,则拷贝一份
    if ([fileManager fileExistsAtPath:sqlFilePath] == NO) {
        NSError * err = nil;
        if ([fileManager copyItemAtPath:originPath toPath:sqlFilePath error:&err] == NO) {
            return nil;
        }
    }
    sqlite3_open([sqlFilePath UTF8String], &db);
    NSLog(@"%@", sqlFilePath);
    return db;
}

//关闭数据库
+(void)closeDB{
    if (db) {
        sqlite3_close(db);
    }
}

-(BOOL)saveData:(NSArray *)data toTable:(NSString *)tableName{
    sqlite3 * sdb = [ZinkDBManager openDB];
    sqlite3_stmt * stmt = nil;
    
    BOOL success = NO;
    
    NSMutableString * strData = [[NSMutableString alloc] init];
    [strData appendFormat:@"insert into %@ values(", tableName];
    for (int i=0; i<data.count-1; i++) {
        [strData appendFormat:@"?,"];
    }

    [strData appendFormat:@"?)"];
    
    const char * cStrData = [strData UTF8String];
    
    int result = sqlite3_prepare_v2(sdb, cStrData, -1, &stmt, nil);
    if (result == SQLITE_OK) {
        for (int i=0; i<data.count; i++) {
            sqlite3_bind_text(stmt, i+1, [[data objectAtIndex:i] UTF8String], -1, nil);
        }
        
        
        if (SQLITE_DONE == sqlite3_step(stmt)) {
            success = YES;
        }
    }
    
    return success;
}

-(BOOL)deleteWithCondition:(NSDictionary *)data fromTable:(NSString *)tableName{
    sqlite3 * sdb = [ZinkDBManager openDB];
    sqlite3_stmt * stmt = nil;
    
    BOOL success = NO;
    
    NSMutableString * strData = [[NSMutableString alloc] init];
    NSArray * arrKeys = [[data allKeys] sortedArrayUsingSelector:@selector(compare:)];
    [strData appendFormat:@"delete from %@ where ", tableName];
    
    [strData appendFormat:@"%@ = ?", [arrKeys objectAtIndex:0]];
    
    for (int i=1; i<arrKeys.count; i++) {
        [strData appendFormat:@"and %@ = ?", [arrKeys objectAtIndex:i]];
    }
    
    const char * cStrData = [strData UTF8String];
    
    
    int result = sqlite3_prepare_v2(sdb, cStrData, -1, &stmt, nil);
    if (result == SQLITE_OK) {
        for (int i=0; i<data.count; i++) {
            NSString * key = [arrKeys objectAtIndex:i];
            sqlite3_bind_text(stmt, i+1, [[data objectForKey:key] UTF8String], -1, nil);
        }
        
        
        if (SQLITE_DONE == sqlite3_step(stmt)) {
            success = YES;
        }
    }
    
    return success;
}

-(BOOL)deleteAllFromTable:(NSString *)tableName{
    sqlite3 * sdb = [ZinkDBManager openDB];
    sqlite3_stmt * stmt = nil;
    
    BOOL success = NO;
    
    NSMutableString * strData = [[NSMutableString alloc] init];
    [strData appendFormat:@"delete from %@", tableName];
    
    const char * cStrData = [strData UTF8String];
    
    
    int result = sqlite3_prepare_v2(sdb, cStrData, -1, &stmt, nil);
    if (result == SQLITE_OK) {
        
        if (SQLITE_DONE == sqlite3_step(stmt)) {
            success = YES;
        }
    }
    
    return success;
}

-(BOOL)updateData:(NSDictionary *)data ByCondition:(NSDictionary *)condition fromTable:(NSString *)tableName{
    sqlite3 * sdb = [ZinkDBManager openDB];
    sqlite3_stmt * stmt = nil;
    BOOL success = NO;

    NSArray * arrDataKeys = [[data allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSArray * arrConditionKeys = [[condition allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    NSMutableString * strData = [[NSMutableString alloc] init];
    [strData appendFormat:@"update %@ set ", tableName];
    for (int i=0; i<arrDataKeys.count-1; i++) {
        [strData appendFormat:@"%@ = ?,", [arrDataKeys objectAtIndex:i]];
    }
    [strData appendFormat:@"%@ = ? where %@ = ?", [arrDataKeys lastObject], [arrConditionKeys objectAtIndex:0]];
    
    for (int i=1; i<arrConditionKeys.count; i++) {
        [strData appendFormat:@"and %@ = ? ", [arrConditionKeys objectAtIndex:i]];
    }

    const char * cStrData = [strData UTF8String];
    
    int result = sqlite3_prepare_v2(sdb, cStrData, -1, &stmt, nil);
    if (result == SQLITE_OK) {
        int j=1;
        
        for (int i=0; i<arrDataKeys.count; i++, j++) {
            sqlite3_bind_text(stmt, j, [[data objectForKey:[arrDataKeys objectAtIndex:i]] UTF8String], -1, nil);
        }
        
        for (int i=0; i<arrConditionKeys.count; i++, j++) {
            sqlite3_bind_text(stmt, j, [[condition objectForKey:[arrConditionKeys objectAtIndex:i]] UTF8String], -1, nil);
        }
        
        
        
        if (SQLITE_DONE == sqlite3_step(stmt)) {
            success = YES;
        }
    }
    
    return success;
    
    
    return YES;
}

-(NSMutableArray *)queryWithCondition:(NSDictionary *)data fromTable:(NSString *)tableName{
    
    sqlite3 * sdb = [ZinkDBManager openDB];
    sqlite3_stmt * stmt = nil;
    NSMutableArray * array = [[NSMutableArray alloc] init];
    
    NSMutableString * strData = [[NSMutableString alloc] init];
    NSArray * arrAllkeys = [[data allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    [strData appendFormat:@"select * from %@ where %@ = ?", tableName, [arrAllkeys objectAtIndex:0]];
    for (int i=1; i<arrAllkeys.count; i++) {
        [strData appendFormat:@"and %@ = ? ", [arrAllkeys objectAtIndex:i]];
    }
    
    const char * cStrData = [strData UTF8String];
    
    int result = sqlite3_prepare_v2(sdb, cStrData, -1, &stmt, nil);
    if (result == SQLITE_OK) {
        
        for (int i=0; i<arrAllkeys.count; i++) {
            const char * cStrInput = [[data objectForKey:[arrAllkeys objectAtIndex:i]] UTF8String];
            sqlite3_bind_text(stmt, i+1, cStrInput, -1, nil);
        }
        
        
        
        while (SQLITE_ROW == sqlite3_step(stmt)) {
            NSMutableArray * arrData = [[NSMutableArray alloc] init];
            
            for (int i=0; i<INT16_MAX; i++) {
                
                const char * cStrResult = (char *)sqlite3_column_text(stmt, i);
                
                
                if (cStrResult == NULL) {
                    break;
                }else{
                    NSString * strResult = [NSString stringWithUTF8String:cStrResult];
                    [arrData addObject:strResult];
                }
                
            }

            [array addObject:arrData];
            
        }
    }
    
    return array;
    
} 
-(NSMutableArray *)queryAllFromTable:(NSString *)tableName{
    sqlite3 * sdb = [ZinkDBManager openDB];
    sqlite3_stmt * stmt = nil;
    NSMutableArray * array = [[NSMutableArray alloc] init];
    
    NSMutableString * strData = [[NSMutableString alloc] init];
    
    [strData appendFormat:@"select * from %@", tableName];

    
    const char * cStrData = [strData UTF8String];
    
    int result = sqlite3_prepare_v2(sdb, cStrData, -1, &stmt, nil);
    if (result == SQLITE_OK) {
        
        while (SQLITE_ROW == sqlite3_step(stmt)) {
            NSMutableArray * arrData = [[NSMutableArray alloc] init];
            
            for (int i=0; i<INT16_MAX; i++) {
                
                const char * cStrResult = (char *)sqlite3_column_text(stmt, i);
                
                
                if (cStrResult == NULL) {
                    break;
                }else{
                    NSString * strResult = [NSString stringWithUTF8String:cStrResult];
                    [arrData addObject:strResult];
                }
                
            }
            
            [array addObject:arrData];
            
        }
    }
    
    return array;
}

@end
